<?php

spl_autoload_register(static function ($class) {
    $path = str_replace(["Evolution", "\\"], [__DIR__ , DIRECTORY_SEPARATOR], $class);
    if (is_file($file = $path . '.php')) {
        require_once $file;
    }
});
