<?php
namespace Evolution\Mammals\Predators\Cats\BigCats;
use Evolution\Mammals\Predators\Cat;

abstract class BigCat extends Cat{
    public function growl(){
        echo "я умею рычать";
    }
}