<?php


namespace Evolution\Mammals;


use Evolution\Live;

// класс млекопитающие 
abstract class Mammal extends Live
{
    // ВСЕ млекопитающие живородящие, поэтому реализация этого метода будет общей для всех
    public function reproduce()
    {
        echo "рожает живых детенышей";
    }

    // так же ВСЕ млекопитающие вскармливают детнышей молоком
    public function milkFeed()
    {
        echo "кормит детенышей молоком";
    }
}