<?php
namespace Evolution\Arthropods\Insects;
use Evolution\Live;

abstract class Insect extends Live{
    public function reproduce()
    {
        echo " из оплодотворённых яиц развиваются личинки, которые растут, 
        развиваются и превращаются во взрослых особей.";
    }

}
